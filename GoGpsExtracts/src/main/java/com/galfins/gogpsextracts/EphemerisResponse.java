package com.galfins.gogpsextracts;

import java.util.List;

public class EphemerisResponse {
    public final List<GnssEphemeris> ephList;
    public final Ephemeris.IonosphericModelProto ionoProto;
    public final Ephemeris.IonosphericModelProto ionoProto2;

    public EphemerisResponse(List<GnssEphemeris> ephList, Ephemeris.IonosphericModelProto ionoProto, Ephemeris.IonosphericModelProto ionoProto2) {
        this.ephList = ephList;
        this.ionoProto = ionoProto;
        this.ionoProto2 = ionoProto2;
    }
}