//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.galfins.gogpsextracts.Bds;

import com.galfins.gogpsextracts.KeplerianEphemeris;

public class BdsEphemeris extends KeplerianEphemeris {
    public final double accuracyM;
    public final double tgdS;
    public final int iodc;

    private BdsEphemeris(BdsEphemeris.Builder builder) {
        super(builder);
        this.accuracyM = builder.accuracyM;
        this.tgdS = builder.tgdS;
        this.iodc = builder.iodc;
    }

    public static BdsEphemeris.Builder newBuilder() {
        return new BdsEphemeris.Builder();
    }

    public static class Builder extends KeplerianEphemeris.Builder<BdsEphemeris.Builder> {
        private double accuracyM;
        private double tgdS;
        private int iodc;

        private Builder() {
        }

        public BdsEphemeris.Builder getThis() {
            return this;
        }

        public BdsEphemeris.Builder setAccuracyM(double accuracyM) {
            this.accuracyM = accuracyM;
            return this.getThis();
        }

        public BdsEphemeris.Builder setTgdS(double tgdS) {
            this.tgdS = tgdS;
            return this.getThis();
        }

        public BdsEphemeris.Builder setIodc(int iodc) {
            this.iodc = iodc;
            return this.getThis();
        }

        public BdsEphemeris build() {
            return new BdsEphemeris(this);
        }
    }
}
