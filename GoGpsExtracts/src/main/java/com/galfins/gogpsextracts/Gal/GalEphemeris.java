package com.galfins.gogpsextracts.Gal;

import com.galfins.gogpsextracts.KeplerianEphemeris;

public class GalEphemeris extends KeplerianEphemeris {
    public final double sisaM;
    public final double tgdS;
    public final boolean isINav;

    private GalEphemeris(GalEphemeris.Builder builder) {
        super(builder);
        this.sisaM = builder.sisaM;
        this.tgdS = builder.tgdS;
        this.isINav = builder.isINav;
    }

    public static GalEphemeris.Builder newBuilder() {
        return new GalEphemeris.Builder();
    }

    public static class Builder extends KeplerianEphemeris.Builder<GalEphemeris.Builder> {
        private double sisaM;
        private double tgdS;
        private boolean isINav;

        private Builder() {
        }

        public GalEphemeris.Builder getThis() {
            return this;
        }

        public GalEphemeris.Builder setSisaM(double sisaM) {
            this.sisaM = sisaM;
            return this.getThis();
        }

        public GalEphemeris.Builder setTgdS(double tgdS) {
            this.tgdS = tgdS;
            return this.getThis();
        }

        public GalEphemeris.Builder setIsINav(boolean isINav) {
            this.isINav = isINav;
            return this.getThis();
        }

        public GalEphemeris build() {
            return new GalEphemeris(this);
        }
    }
}
